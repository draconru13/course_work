﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace work
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            button1.Visible = false;
            button2.Visible = false;
            button3.Visible = false;
            button4.Visible = false;
            button5.Visible = false;
            button6.Visible = false;
            button7.Visible = false;
            button8.Visible = false;
            button9.Visible = false;
            button10.Visible = true;
            button11.Visible = false;
            button12.Visible = true;
            button13.Visible = false;
            button14.Visible = false;
            button15.Visible = false; 
            button16.Visible = false;

            if (button10.Visible == true)
            {
                DialogResult dialog_icon = MessageBox.Show("Брать информационное сообщение из файла?", "Начало работы", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information);
                if (dialog_icon == DialogResult.Cancel)
                {
                    this.Close();
                }
                if (dialog_icon == DialogResult.No)
                {
                    //сюда написать основную функцию
                    button1.Visible = false;
                    button2.Visible = false;
                    button3.Visible = false;
                    button4.Visible = false;
                    button5.Visible = false;
                    button6.Visible = false;
                    button7.Visible = false;
                    button8.Visible = false;
                    button9.Visible = false;
                    button10.Visible = true;
                    button11.Visible = false;
                    button12.Visible = true;
                    button13.Visible = false;
                    button14.Visible = false;
                    button15.Visible = false;
                    button16.Visible = false;

                    string x = textBox10.Text;
                    Random rnd = new Random();

                    int step1 = 0;
                    for (int i = 0; i < x.Length; i++)
                    {
                        if (x[i] != '0' && x[i] != '1' && x[i] != '2' && x[i] != '3' && x[i] != '4' && x[i] != '5' && x[i] != '6' && x[i] != '7' && x[i] != '8' && x[i] != '9')
                        {
                            textBox10.Text = String.Empty;
                            textBox11.Text = String.Empty;
                            button10.Visible = true;
                            button2.Visible = false;
                            step1++;
                        }
                    }
                    if (step1 > 0)
                    {
                        MessageBox.Show("Введите сообщение меньшей длины", "Невозможно сгенерировать", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    if (textBox10.Text.Length > 0)
                    {
                        for (int y = 0; y < Convert.ToInt32(textBox10.Text); y++)
                        {
                            textBox1.Text += rnd.Next(2).ToString();
                        }
                    }

                }

                if (dialog_icon == DialogResult.Yes)
                {
                    StreamReader sr = new StreamReader("work.txt");
                    string s;
                    string s1 = "";

                    while (sr.EndOfStream != true)
                    {
                        s = sr.ReadLine();
                        for (int i = 0; i < s.Length; i++)
                        {
                            if (s[i] == '1' || s[i] == '0')
                            {
                                textBox1.Text = s;
                                textBox10.Text = Convert.ToString(s.Length);
                            }
                            else if (s[i] != '1' || s[i] != '0')
                            {
                                int y = (int)s[i];
                                string BinaryCode = Convert.ToString(y, 2);
                                s1 += BinaryCode;

                            }

                        }
                    }
                    textBox1.Text = s1;
                    textBox10.Text = Convert.ToString(textBox1.Text.Length);



                    button1.Visible = false;
                    button2.Visible = true;
                    button3.Visible = false;
                    button4.Visible = false;
                    button5.Visible = false;
                    button6.Visible = false;
                    button7.Visible = false;
                    button8.Visible = false;
                    button9.Visible = false;
                    button10.Visible = false;
                    button11.Visible = false;
                    button12.Visible = true;
                    button13.Visible = false;
                    button14.Visible = false;
                    button15.Visible = false;
                    button16.Visible = false;

                    Random rnd = new Random();
                    
                    textBox11.Text = Convert.ToString(rnd.Next(1, 4));
                }
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Visible = false;
            button2.Visible = false;
            button3.Visible = false;
            button4.Visible = true;
            button5.Visible = false;
            button6.Visible = false;
            button7.Visible = false;
            button8.Visible = false;
            button9.Visible = false;
            button10.Visible = false;
            button11.Visible = false;
            button12.Visible = true;
            button13.Visible = true;
            button14.Visible = false;
            button15.Visible = false;
            button16.Visible = false;


            int step = 1;

            if (step > 0)
            {
                textBox2.Text = String.Empty;
                textBox3.Text = String.Empty;
            }
            string message = textBox1.Text;
            if (button2WasClicked && button3WasClicked)
            {

                for (int i = 0; i < message.Length; i++)
                {
                    if (message[i] != '1' && message[i] != '0' && message.Length > 33)
                    {
                        MessageBox.Show("Введите сообщение меньшей длины", "Некорректное сообщение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        textBox1.Text = String.Empty;
                        break;
                    }
                    if (message[i] != '1' && message[i] != '0')
                    {
                        MessageBox.Show("Введите корректное сообщение", "Некорректное сообщение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        textBox1.Text = String.Empty;
                        break;
                    }
                    
                    //дописать обработку пустого значения
                    else if (message.Length == 0)
                    {
                        MessageBox.Show("Введите сообщение", "Некорректное сообщение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        break;
                    }
                }
            }
            else if (!button2WasClicked && button3WasClicked)
            {
                MessageBox.Show("Проверочная матрица не построена", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (!button2WasClicked && !button3WasClicked)
            {
                MessageBox.Show("Матрицы не построены", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (button2WasClicked && !button3WasClicked)
            {
                MessageBox.Show("Порождающая матрица не построена", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (message.Length  == 0)
            {
                MessageBox.Show("Не задано информационное сообщение", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            if (message.Length > 0 && button2WasClicked && button3WasClicked)
            {
                Random rnd = new Random();
                for (int i = 0; i < Convert.ToInt32(textBox10.Text)+7; i++)
                {
                    textBox2.Text += rnd.Next(2).ToString();
                }
            }
            
            if (textBox1.Text.Length == 0)
            {
                MessageBox.Show("Не задано информационное сообщение", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }

            textBox3.Text = textBox2.Text;

            step++;
            if (textBox6.Text.Length > 0 && textBox7.Text.Length > 0)
            {
                button1.Visible = false;
            }



        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
        public static string text1;
        public void textBox2_TextChanged(object sender, EventArgs e)
        {
            text1 = textBox2.Text;   
        }



        private bool button2WasClicked = false;
        public static int[,] array_H;
        public static int[,] array_G1;
        public static int[,] array_G2;
        public static double r;
        public static double n;
        public static double k;

        private bool button10WasClicked = false;


        double exeption;
        double t;
        double n1;
        //строится проверочная матрица
        public void button2_Click(object sender, EventArgs e)
        {
            button1.Visible = false;
            button2.Visible = false;
            button3.Visible = true;
            button4.Visible = false;
            button5.Visible = false;
            button6.Visible = true;
            button7.Visible = false;
            button8.Visible = false;
            button9.Visible = false;
            button10.Visible = false;
            button11.Visible = false;
            button12.Visible = true;
            button13.Visible = false;
            button14.Visible = false;
            button15.Visible = false;
            button16.Visible = false;


            if (textBox1.Text.Length == 0)
            {
                MessageBox.Show("Не задано информационное сообщение", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                textBox3.Text = textBox2.Text;

                button2WasClicked = true;
                Random rnd = new Random();

                int q = (int)numericUpDown1.Value; // задаётся пользователем
                int m = 6;
                n = Math.Pow(q, m) - 1;
                n1 = n / m;
                t = rnd.Next(q, (int)n1);
                r = m * t; //для проверочной матрицы
                exeption = t / 2; //вектор ошибок
                if (exeption == 0)
                {
                    exeption = 1;   
                }

                array_H = new int[(int)r, (int)n];
                for (int x = 0; x < r; x++)
                {
                    for (int y = 0; y < n; y++)
                    {
                        array_H[x, y] = rnd.Next(2);
                    }
                }


                for (int i = 0; i < r; i++)
                {
                    for (int j = 0; j < n - 1; j++)
                    {
                        textBox6.Text += array_H[i, j].ToString();
                        if (j == n - 2)
                        {
                            textBox6.Text += Environment.NewLine;
                        }
                    }
                }
            }


            if (textBox1.Text.Length > 0)
            {
                button2.Visible = false;
            }

        }
        private bool button3WasClicked = false;


        private void button10_Click(object sender, EventArgs e)
        {
            button1.Visible = false;
            button2.Visible = true;
            button3.Visible = false;
            button4.Visible = false;
            button5.Visible = false;
            button6.Visible = false;
            button7.Visible = false;
            button8.Visible = false;
            button9.Visible = false;
            button10.Visible = false;
            button11.Visible = false;
            button12.Visible = true;
            button13.Visible = false;
            button14.Visible = false;
            button15.Visible = false;
            button16.Visible = false;


            textBox11.Text = Convert.ToString(exeption);


            int step = 1;
            string x = textBox10.Text;


            for (int i = 0; i < x.Length; i++)
            {
                if (x[i] != '0' && x[i] != '1' && x[i] != '2' && x[i] != '3' && x[i] != '4' && x[i] != '5' && x[i] != '6' && x[i] != '7' && x[i] != '8' && x[i] != '9')
                {
                    textBox10.Text = String.Empty;
                    textBox11.Text = String.Empty;
                    button10.Visible = true;
                    button2.Visible = false;
                }
                else
                {
                    x = textBox10.Text;
                }
                
            }

            if (x.Length > 0)
            {
                button10WasClicked = true;
                Random rnd = new Random();

                for (int i = 0; i < x.Length; i++)
                {
                    if (x[i] != '0' && x[i] != '1' && x[i] != '2' && x[i] != '3' && x[i] != '4' && x[i] != '5' && x[i] != '6' && x[i] != '7' && x[i] != '8' && x[i] != '9')
                    {
                        //MessageBox.Show("Введите количество символов", "Некорректные данные", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        textBox10.Text = String.Empty;
                        textBox11.Text = String.Empty;
                        button10.Visible = true;
                        button2.Visible = false;
                    }
                }
                if (textBox10.Text.Length > 0)
                {
                    for (int y = 0; y < Convert.ToInt32(textBox10.Text); y++)
                    {
                        textBox1.Text += rnd.Next(2).ToString();
                    }
                }
            }
            else if (x.Length == 0)
            {
                MessageBox.Show("Введите количество символов", "Некорректное сообщение", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            step++;

            if (textBox1.Text.Length > 0)
            {
                Random rnd = new Random();

                int q = (int)numericUpDown1.Value; // задаётся пользователем
                int m = 6;
                n = Math.Pow(q, m) - 1;
                n1 = n / m;
                t = rnd.Next(q, (int)n1);
                r = m * t; //для проверочной матрицы
                exeption = t / 2; //вектор ошибок

                button10.Visible = false;
                textBox11.Text = Convert.ToString(Convert.ToUInt32(exeption));
            }


        }



        //строится порождающая матрица
        private void button3_Click(object sender, EventArgs e)
        {
            button1.Visible = true;
            button2.Visible = false;
            button3.Visible = true;
            button4.Visible = false;
            button5.Visible = false;
            button6.Visible = true;
            button7.Visible = true;
            button8.Visible = false;
            button9.Visible = false;
            button10.Visible = false;
            button11.Visible = false;
            button12.Visible = true;
            button13.Visible = false;
            button14.Visible = false;
            button15.Visible = false;
            button16.Visible = false;


            if (textBox6.Text.Length == 0)
            {
                MessageBox.Show("Не задана проверочная матрица", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                button3WasClicked = true;
                //количество информационных символов
                k = n - r;

                Random rnd = new Random();
                array_G1 = new int[(int)k, (int)k];
                array_G2 = new int[(int)k, (int)(n - k)];


                for (int x = 0; x < k; x++)
                {
                    for (int y = 0; y < k; y++)
                    {
                        array_G1[x, y] = 0;
                        if (x == y)
                        {
                            array_G1[x, y] = 1;
                        }
                    }
                }

                for (int i1 = 0; i1 < k; i1++)
                {
                    for (int j1 = 0; j1 < k; j1++)
                    {
                        textBox7.Text += array_G1[i1, j1].ToString();
                        if (j1 == k - 1)
                        {
                            textBox7.Text += Environment.NewLine;
                        }
                    }
                }

                array_G2 = new int[(int)k, (int)n];
                for (int x1 = 0; x1 < k; x1++)
                {
                    for (int y1 = 0; y1 < n - k; y1++)
                    {
                        array_G2[x1, y1] = rnd.Next(2);
                    }
                }
            }


            if (textBox6.Text.Length > 0)
            {
                button3.Visible = false;
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }


        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }
        public static string text3;
        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            text3 = textBox5.Text;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            int step = 1;

            if (textBox2.Text != textBox5.Text)
            {
                MessageBox.Show("Декодирование выполнено неверно", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Random rnd = new Random();
                for (int i = 0; i < Convert.ToInt32(textBox10.Text); i++)
                {
                    textBox9.Text += rnd.Next(2).ToString();
                }
                button5.Visible = false;
                label12.Text = "Неверное сообщение";

                Form ifrm = new Form4();
                ifrm.Show(); // отображаем Form4
            }
            else
            {
                if (step > 0)
                {
                    textBox9.Text = String.Empty;
                }
                if (textBox5.Text.Length > 0)
                {
                    textBox9.Text = textBox1.Text;
                }
                step++;


                if (textBox5.Text.Length > 0 && textBox2.Text.Length > 0)
                {
                    button5.Visible = false;
                }
                button15.Visible = false;
                button16.Visible = true;
            }
            
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }
        public static string text2;
        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            text2 = textBox4.Text;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            button1.Visible = false;
            button2.Visible = false;
            button3.Visible = false;
            button4.Visible = false;
            button5.Visible = false;
            button6.Visible = false;
            button7.Visible = false;
            button8.Visible = false;
            button9.Visible = false;
            button10.Visible = false;
            button11.Visible = false;
            button12.Visible = true;
            button13.Visible = false;
            button14.Visible = true;
            button15.Visible = false;

            string x1 = textBox2.Text;
            string x2 = textBox3.Text;
            int exep = 0;
            int step = 1;

            if (step > 0)
            {
                textBox4.Text = String.Empty;
            }

            if (textBox3.Text == textBox2.Text)
            {
                textBox8.Text = "0";
                for (int i = 0; i < x1.Length; i++)
                {
                    textBox4.Text += "0";
                }
            }
            else if (x1.Length > x2.Length && x1.Length < x2.Length)
            {
                MessageBox.Show("Нельзя изменять количество символов", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            else if (x1.Length == x2.Length)
            {
                for (int i = 0; i < x1.Length; i++)
                {
                    if (x1[i] == x2[i])
                    {
                        textBox4.Text += "0";
                    }
                    else if (x1[i] != x2[i])
                    {
                        textBox4.Text += "1";
                        exep ++;
                    }
                }
            }
            textBox8.Text = exep.ToString();

            step++;


            if (textBox2.Text.Length > 0)
            {
                button1.Visible = false;
            }

            DialogResult dialog_icon = MessageBox.Show("Вы хотите повтороно посчитать вектор ошибок?", "Предупреждение", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (dialog_icon == DialogResult.No)
            {
                button11.Visible = true;
            }
            if (dialog_icon == DialogResult.Yes)
            {
                button11.Visible = false;
                button4.Visible = true;
            }

        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (textBox6.Text.Length <= 0)
            {
                MessageBox.Show("Пустая проверочная матрица", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                Form ifrm = new Form2();
                ifrm.Show(); // отображаем Form2
            }

        }

        private void button7_Click(object sender, EventArgs e)
        {

            if (textBox7.Text.Length <= 0)
            {
                MessageBox.Show("Пустая порождающая матрица", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                Form ifrm = new Form3();
                ifrm.Show(); // отображаем Form3
            }
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {
            textBox6.Text = String.Empty;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            textBox7.Text = String.Empty;
        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void textBox10_TextChanged(object sender, EventArgs e)
        {
            ActiveControl = null;
        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        private void button11_Click(object sender, EventArgs e)
        {
            button1.Visible = false;
            button2.Visible = false;
            button3.Visible = false;
            button4.Visible = false;
            button5.Visible = true;
            button6.Visible = false;
            button7.Visible = false;
            button8.Visible = false;
            button9.Visible = false;
            button10.Visible = false;
            button11.Visible = false;
            button12.Visible = true;
            button13.Visible = false;
            button14.Visible = false;
            button15.Visible = true;
            button16.Visible = false;

            textBox10.TabStop = false;
            ActiveControl = null;

            int step = 1;
            if (Convert.ToInt32(textBox8.Text) <= Convert.ToInt32(textBox11.Text))
            {
                if (step > 0)
                {
                    textBox5.Text = String.Empty;
                }

                if (textBox4.Text.Length > 0)
                {
                    textBox5.Text = textBox2.Text;
                }
                step++;

                if (textBox4.Text.Length > 0)
                {
                    button11.Visible = false;
                    button4.Visible = false;
                }

            }
            else if(Convert.ToInt32(textBox8.Text) > Convert.ToInt32(textBox11.Text))
            {

                MessageBox.Show("Превышено допустимое количество ошибок. Декодирование будет неверно", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Random rnd = new Random();
                for (int i = 0; i < Convert.ToInt32(textBox10.Text); i++)
                {
                    textBox5.Text += rnd.Next(2).ToString();
                }
                label6.Text = "Неверно исправленное слово";
                button11.Visible = false;
                button4.Visible = false;
            }


        }
        public static string text4;
        private void textBox9_TextChanged(object sender, EventArgs e)
        {
            text4 = textBox9.Text;
        }

        private void button12_Click(object sender, EventArgs e)
        {
            textBox1.Text = String.Empty;
            textBox2.Text = String.Empty;
            textBox3.Text = String.Empty;
            textBox4.Text = String.Empty;
            textBox5.Text = String.Empty;
            textBox6.Text = String.Empty;
            textBox7.Text = String.Empty;
            textBox8.Text = String.Empty;
            textBox9.Text = String.Empty;
            textBox10.Text = String.Empty;
            textBox11.Text = String.Empty;


            button1.Visible = false;
            button2.Visible = false;
            button3.Visible = false;
            button4.Visible = false;
            button5.Visible = false;
            button6.Visible = false;
            button7.Visible = false;
            button8.Visible = false;
            button9.Visible = false;
            button10.Visible = true;
            button11.Visible = false;
            button12.Visible = true;
            button13.Visible = false;
            button14.Visible = false;
            button15.Visible = false;
            button16.Visible = false;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void textBox11_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void button13_Click(object sender, EventArgs e)
        {
            Form ifrm = new Form5();
            ifrm.Show(); // отображаем Form5
        }

        private void button14_Click(object sender, EventArgs e)
        {
            Form ifrm = new Form6();
            ifrm.Show(); // отображаем Form6
        }

        private void button15_Click(object sender, EventArgs e)
        {
            Form ifrm = new Form7();
            ifrm.Show(); // отображаем Form7
        }

        private void button16_Click(object sender, EventArgs e)
        {
            Form ifrm = new Form8();
            ifrm.Show(); // отображаем Form8
        }
    }
}
