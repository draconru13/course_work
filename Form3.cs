﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace work
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
            this.Text = "Порождающая матрица G с размерами " + Form1.k + " на " + Form1.n;
        }

        private void Form3_Load(object sender, EventArgs e)
        {

            for (int i = 0; i < Form1.k; i++)
            {
                for (int j = 0; j < Form1.k; j++)
                {
                    textBox1.Text += Form1.array_G1[i, j].ToString();
                    if (j == Form1.k - 1)
                    {
                        textBox1.Text += Environment.NewLine;
                    }
                }
            }

            for (int i1 = 0; i1 < Form1.k; i1++)
            {
                for (int j1 = 0; j1 < Form1.n-Form1.k-1; j1++)
                {
                    textBox2.Text += Form1.array_G2[i1, j1].ToString();
                    if (j1 == Form1.n-Form1.k-2)
                    {
                        textBox2.Text += Environment.NewLine;
                    }
                }
            }
            ActiveControl = null;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
